// Copyright Claudio Mattera 2020.
// Distributed under the MIT License.
// See accompanying file License.txt, or online at
// https://opensource.org/licenses/MIT

use log::*;

use std::path::Path;

use clap::{app_from_crate, crate_authors, crate_description, crate_name, crate_version};
use clap::{Arg, ArgMatches};

use regex::Regex;

use num_complex::Complex;

use julia::create_juliaset_image;

fn main() {
    let arguments = parse_arguments();
    setup_logging(arguments.occurrences_of("verbosity"));

    let constant = arguments
        .value_of("constant")
        .map(to_complex)
        .expect("Missing complex constant")
        .expect("Invalid complex constant");
    let x_domain = arguments
        .value_of("x_domain")
        .map(to_domain)
        .expect("Missing X domain")
        .expect("Invalid X domain");
    let y_domain = arguments
        .value_of("y_domain")
        .map(to_domain)
        .expect("Missing Y domain")
        .expect("Invalid Y domain");
    let n = arguments
        .value_of("size")
        .map(|s| s.parse::<usize>())
        .expect("Missing size")
        .expect("Invalid size");
    let iterations = arguments
        .value_of("iterations")
        .map(|s| s.parse::<u32>())
        .expect("Missing iterations")
        .expect("Invalid iterations");
    let path = arguments
        .value_of("path")
        .map(Path::new)
        .expect("Missing path");
    let m = (n as f64 * (y_domain.1 - y_domain.0) / (x_domain.1 - x_domain.0)) as usize;

    info!("Creating Julia set image with the following parameters");
    info!("Complex constant: {}", constant);
    info!("X domain: [{}, {}], Y domain: [{}, {}]", x_domain.0, x_domain.1, y_domain.0, y_domain.1);
    info!("Iterations: {}", iterations);
    info!("Image size: {}x{}", n, m);

    let image = create_juliaset_image(n, m, x_domain, y_domain, constant, iterations);
    image.save(path).expect("Save error");
}

fn parse_arguments() -> ArgMatches<'static> {
    app_from_crate!()
        .arg(
            Arg::with_name("verbosity")
                .short("v")
                .long("verbose")
                .multiple(true)
                .help("Set the level of verbosity"),
        )
        .arg(
            Arg::with_name("x_domain")
                .short("x")
                .long("x-domain")
                .default_value("-1:1")
                .validator(is_domain)
                .help("Set the X domain"),
        )
        .arg(
            Arg::with_name("y_domain")
                .short("y")
                .long("y-domain")
                .default_value("-1:1")
                .validator(is_domain)
                .help("Set the Y domain"),
        )
        .arg(
            Arg::with_name("constant")
                .short("c")
                .long("constant")
                .default_value("-0.8+0.156i")
                .validator(is_complex)
                .help("Set the complex constant"),
        )
        .arg(
            Arg::with_name("size")
                .short("s")
                .long("size")
                .default_value("200")
                .help("Set the horizontal image size"),
        )
        .arg(
            Arg::with_name("iterations")
                .short("i")
                .long("iterations")
                .default_value("60")
                .help("Set the number of iterations"),
        )
        .arg(
            Arg::with_name("path")
                .short("p")
                .long("path")
                .default_value("juliaset.png")
                .help("Image path"),
        )
        .get_matches()
}

fn is_domain(text: String) -> Result<(), String> {
    to_domain(&text).map(|_| ())
}

fn to_domain(text: &str) -> Result<(f64, f64), String> {
    let re = Regex::new(r"^([+-]?(?:[0-9]*[\.])?[0-9]+):([+-]?(?:[0-9]*[\.])?[0-9]+)$")
        .expect("Invalid regular expression");

    match re.captures(&text) {
        None => Err("Invalid range".to_owned()),
        Some(captures) => {
            let min = captures.get(1).unwrap().as_str().parse::<f64>().expect("Invalid number");
            let max = captures.get(2).unwrap().as_str().parse::<f64>().expect("Invalid number");
            if min >= max {
                Err("Empty range".to_owned())
            } else {
                Ok((min, max))
            }
        }
    }
}

fn is_complex(text: String) -> Result<(), String> {
    to_complex(&text).map(|_| ())
}

fn to_complex(text: &str) -> Result<Complex<f64>, String> {
    let re = Regex::new(r"^([+-]?(?:[0-9]*[\.])?[0-9]+)([+-](?:[0-9]*[\.])?[0-9]+)[ij]$")
        .expect("Invalid regular expression");

    match re.captures(&text) {
        None => Err("Invalid complex constant".to_owned()),
        Some(captures) => {
            let real = captures.get(1).unwrap().as_str().parse::<f64>().expect("Invalid number");
            let imaginary = captures.get(2).unwrap().as_str().parse::<f64>().expect("Invalid number");
            Ok(Complex::new(real, imaginary))
        }
    }
}

fn setup_logging(verbosity: u64) {
    let default_log_filter = match verbosity {
        0 => "warn",
        1 => "info",
        2 => "info,julia=debug",
        3 | _ => "debug",
    };
    let filter = env_logger::Env::default().default_filter_or(default_log_filter);
    env_logger::Builder::from_env(filter).format_timestamp(None).init();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn is_domain_correct() {
        let result = is_domain("-1:3".to_owned());
        assert_eq!(result, Ok(()));
    }

    #[test]
    fn is_domain_empty_range() {
        let result = is_domain("3.0:2".to_owned());
        assert_eq!(result, Err("Empty range".to_owned()));
    }

    #[test]
    fn is_domain_invalid_range() {
        let result = is_domain("liuie".to_owned());
        assert_eq!(result, Err("Invalid range".to_owned()));
    }

    #[test]
    fn to_domain_correct() {
        let result = to_domain("-1:3");
        assert_eq!(result, Ok((-1.0, 3.0)));
    }

    #[test]
    fn to_domain_empty_range() {
        let result = to_domain("3.0:2");
        assert_eq!(result, Err("Empty range".to_owned()));
    }

    #[test]
    fn to_domain_invalid_range() {
        let result = is_domain("liuie".to_owned());
        assert_eq!(result, Err("Invalid range".to_owned()));
    }

    #[test]
    fn is_complex_correct_i() {
        let result = is_complex("-1+3i".to_owned());
        assert_eq!(result, Ok(()));
    }

    #[test]
    fn is_complex_correct_j() {
        let result = is_complex("-1+3j".to_owned());
        assert_eq!(result, Ok(()));
    }

    #[test]
    fn is_complex_invalid_range() {
        let result = is_complex("liuie".to_owned());
        assert_eq!(result, Err("Invalid complex constant".to_owned()));
    }

    #[test]
    fn to_complex_correct_i() {
        let result = to_complex("-1+3i");
        assert_eq!(result, Ok(Complex::new(-1.0, 3.0)));
    }

    #[test]
    fn to_complex_correct_j() {
        let result = to_complex("-1+3j");
        assert_eq!(result, Ok(Complex::new(-1.0, 3.0)));
    }

    #[test]
    fn to_complex_correct_other() {
        let result = to_complex("-0.794589-0.484529i");
        assert_eq!(result, Ok(Complex::new(-0.794589, -0.484529)));
    }

    #[test]
    fn to_complex_invalid_range() {
        let result = to_complex("liuie");
        assert_eq!(result, Err("Invalid complex constant".to_owned()));
    }
}
