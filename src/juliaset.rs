// Copyright Claudio Mattera 2020.
// Distributed under the MIT License.
// See accompanying file License.txt, or online at
// https://opensource.org/licenses/MIT

use log::*;

use num_complex::Complex;

use num_traits::clamp_max;

use image::{ImageBuffer, Rgb, RgbImage};

use crate::matrix::Matrix;
use crate::colormap::value_to_heat;

/// Create an image of a Julia set
///
/// ````
/// # use julia::create_juliaset_image;
/// # use num_complex::Complex;
/// let (n, m) = (150, 100);
/// let x_domain = (-1.5, 1.5);
/// let y_domain = (-1.0, 1.0);
/// let c = Complex::new(-0.8, 0.156);
/// let iterations = 60;
/// let image = create_juliaset_image(n, m, x_domain, y_domain, c, iterations);
/// ````
pub fn create_juliaset_image(
            n: usize,
            m: usize,
            x_domain: (f64, f64),
            y_domain: (f64, f64),
            constant: Complex<f64>,
            iterations: u32,
        ) -> RgbImage {
    debug!("Creating plane");
    let plane = create_plane(n, m, x_domain, y_domain);

    debug!("Creating Julia set set");
    let julia_set = create_julia_set(
        plane,
        |z, constant| z.powi(2) + constant,
        constant,
        iterations,
    );

    debug!("Creating image");
    create_image(julia_set)
}

fn create_plane(
            n: usize,
            m: usize,
            x_domain: (f64, f64),
            y_domain: (f64, f64),
        ) -> Matrix<Complex<f64>> {
    let x_range = x_domain.1 - x_domain.0;
    let y_range = y_domain.1 - y_domain.0;

    let mut matrix = Matrix::new(Complex::new(0.0, 0.0), (n, m));
    for row in 0..n {
        for column in 0..m {
            let x_in_0_1 = (row as f64) / (n as f64 - 1.0);
            let y_in_0_1 = (column as f64) / (m as f64 - 1.0);
            let x_in_range = x_in_0_1 * x_range + x_domain.0;
            let y_in_range = y_in_0_1 * y_range + y_domain.0;
            let z = Complex::new(x_in_range, y_in_range);
            matrix.set((row, column), z);
        }
    }

    matrix
}

fn create_julia_set(
            plane: Matrix<Complex<f64>>,
            function: fn(Complex<f64>, Complex<f64>) -> Complex<f64>,
            constant: Complex<f64>,
            iterations: u32,
        ) -> Matrix<f64> {
    let (n, m) = plane.size();

    let mut julia_set = Matrix::new(0.0, (n, m));

    for row in 0..n {
        for column in 0..m {
            let mut z = plane.get((row, column));

            for _ in 0..iterations {
                z = function(z, constant);
            }

            let value = clamp_max(z.norm(), 1.0);

            julia_set.set((row, column), value);
        }
    }

    julia_set
}

fn create_image(julia_set: Matrix<f64>) -> RgbImage {
    let (n, m) = julia_set.size();

    ImageBuffer::from_fn(
        n as u32,
        m as u32,
        |x, y| {
            let value = julia_set.get((x as usize, y as usize));
            let (red, green, blue, _) = value_to_heat(value);
            Rgb([red, green, blue])
        }
    )
}
