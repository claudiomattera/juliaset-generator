// Copyright Claudio Mattera 2020.
// Distributed under the MIT License.
// See accompanying file License.txt, or online at
// https://opensource.org/licenses/MIT

use num_traits::Num;

/// A matrix, i.e. a grid of numbers
#[derive(Debug)]
pub struct Matrix<T> {
    n: usize,
    m: usize,
    data: Vec<T>,
}

impl <T: Num + Clone + Copy> Matrix<T> {
    /// Create a new matrix with given size
    pub fn new(value: T, (n, m): (usize, usize)) -> Self {
        Matrix {
            n,
            m,
            data: vec![value; n * m],
        }
    }

    /// Return the matrix size
    pub fn size(self: &Self) -> (usize, usize) {
        (self.n, self.m)
    }

    /// Set the value in one of the matrix cells
    pub fn set(self: &mut Self, coordinates: (usize, usize), value: T) {
        let i = self.xy_to_index(coordinates);
        self.data[i] = value;
    }

    /// Get the value in one of the matrix cells
    pub fn get(self: &Self, coordinates: (usize, usize)) -> T {
        let i = self.xy_to_index(coordinates);
        self.data[i]
    }

    fn xy_to_index(self: &Self, (x, y): (usize, usize)) -> usize {
        (x * self.m + y) as usize
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    use num_complex::Complex;

    #[test]
    fn matrix_new() {
        let n = 4;
        let m = 6;
        let matrix = Matrix::new(0, (n, m));
        for r in 0..n {
            for c in 0..m {
                assert_eq!(matrix.get((r, c)), 0);
            }
        }
    }

    #[test]
    fn matrix_size() {
        let n = 4;
        let m = 6;
        let matrix = Matrix::new(0, (n, m));
        assert_eq!(matrix.size(), (n, m));
    }

    #[test]
    fn matrix_set() {
        let n = 4;
        let m = 6;
        let mut matrix = Matrix::new(0, (n, m));
        matrix.set((2, 1), 8);
        assert_eq!(matrix.get((2, 1)), 8);
    }

    #[test]
    fn matrix_complex() {
        let n = 4;
        let m = 6;
        let mut matrix = Matrix::new(Complex::new(0.0, 0.0), (n, m));
        matrix.set((2, 1), Complex::new(8.0, 4.0));
        assert_eq!(matrix.get((2, 1)), Complex::new(8.0, 4.0));
    }
}
