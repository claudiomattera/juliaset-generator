// Copyright Claudio Mattera 2020.
// Distributed under the MIT License.
// See accompanying file License.txt, or online at
// https://opensource.org/licenses/MIT

mod colormap;
mod matrix;
mod juliaset;

pub use juliaset::create_juliaset_image;
