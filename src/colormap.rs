// Copyright Claudio Mattera 2020.
// Distributed under the MIT License.
// See accompanying file License.txt, or online at
// https://opensource.org/licenses/MIT

pub fn value_to_heat(value: f64) -> (u8, u8, u8, u8) {
    let alpha = value.sqrt();
    let (red, green, blue) = match value {
        value if 0.0 <= value && value <= 1.0 / 8.0 => (0.0, 0.0, 4.0 * value + 0.5),
        value if 1.0 / 8.0 < value && value <= 3.0 / 8.0 => (0.0, 4.0 * value - 0.5, 1.0),
        value if 3.0 / 8.0 < value && value <= 5.0 / 8.0 => (4.0 * value - 1.5, 1.0, -4.0 * value + 2.5),
        value if 5.0 / 8.0 < value && value <= 7.0 / 8.0 => (1.0, -4.0 * value + 3.5, 0.0),
        value if 7.0 / 8.0 < value && value <= 1.0 => (-4.0 * value + 4.5, 0.0, 0.0),
        _value => (0.0, 0.0, 0.0),
    };
    ((255.0 * red) as u8, (255.0 * green) as u8, (255.0 * blue) as u8, (255.0 * alpha) as u8)
}
