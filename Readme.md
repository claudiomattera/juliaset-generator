Julia Set Generator
====

This is a [Rust] application that generates colourful images of [Julia sets].

<https://gitlab.com/claudiomattera/juliaset-generator/>

![Julia set](images/-0.8+0.156i.png)

Installation
----

Executables for Linux and Windows can be found in the [releases page](https://gitlab.com/claudiomattera/juliaset-generator/-/releases).


### From source

This application can be compiled using the Rust toolchain.

~~~~shell
cargo build --release
~~~~

The resulting executable will be created in `target/release/julia`.


Usage
----

This is a command-line application.
It can be configured using command-line arguments, as described in the help message:

~~~~plain
> julia --help
juliaset-generator 0.1.0
Claudio Mattera <claudio@mattera.it>
An application to generate colourful pictures of Julia sets

USAGE:
    julia [FLAGS] [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information
    -v, --verbose    Set the level of verbosity

OPTIONS:
    -c, --constant <constant>        Set the complex constant [default: -0.8+0.156i]
    -i, --iterations <iterations>    Set the number of iterations [default: 60]
    -p, --path <path>                Image path [default: juliaset.png]
    -s, --size <size>                Set the horizontal image size [default: 200]
    -x, --x-domain <x_domain>        Set the X domain [default: -1:1]
    -y, --y-domain <y_domain>        Set the Y domain [default: -1:1]
~~~~


### Examples

The image at the top of this page was generated using the following command

~~~~shell
julia --x-domain="-1.5:1.5" --size=600
~~~~

The generated image depends mostly on the value of the complex constant, which can be specified from command line:

~~~~shell
julia --constant="+0.378243+0.207300i" -v --y-domain="-1.2:1.2" --size=600
~~~~

![Julia set](images/+0.378243+0.207300i.png)


License
----

Copyright Claudio Mattera 2020

You are free to copy, modify, and distribute this application with attribution under the terms of the [MIT license]. See the [`License.txt`](./License.txt) file for details.


[Julia sets]: https://en.wikipedia.org/wiki/Julia_set
[Rust]: https://rust-lang.org/
[MIT license]: https://opensource.org/licenses/MIT
